const nodemailer = require("nodemailer");

let transporter = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  secure: false, // true for 465, false for other ports
  auth: {
    user: "269058634a56e1", // generated ethereal user
    pass: "1e290915979cf7", // generated ethereal password
  },
});
const sendEmail = (data) => {
  //All info require for sending mail
  //{to:"santosh.subedi@gmail.com",subject:"hello there",text:"how have you been",text:"k xa khabar",html="<p>k xa khabar</p>"}
  //PS: text is overwritten by html
  transporter.sendMail({
    from: process.env.EMAIL_USERNAME, // sender address
    to: data.to, // list of receivers
    subject: data.subject, // Subject line
    text: data.text, // plain text body
    html: data.html, // html body
  });
};

module.exports = sendEmail;
