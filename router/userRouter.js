const express = require("express");
const {
  signup,
  login,
  protect,
  getMe,
} = require("../controller/authController");
const { getAllUsers, getUser } = require("../controller/userController");
const router = express.Router();

router.post("/signup", signup);
router.post("/login", login);
router.get("/me", protect, getMe);

router.route("/").get(getAllUsers);

module.exports = router;
