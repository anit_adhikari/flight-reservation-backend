const express = require("express");
const { protect } = require("../controller/authController");
const { getCheckoutSession } = require("../controller/bookingController");
const router = express.Router();

router
  .route("/checkout-session/:bookingId/:noOfPassanger")
  .get(protect, getCheckoutSession);

module.exports = router;
