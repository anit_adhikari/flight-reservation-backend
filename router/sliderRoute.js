const express = require("express");
const {
  createSlider,
  deleteSlider,
  getAllSlider,
  uploadSliderImages,
  resizeSliderImage,
} = require("../controller/sliderController");

const router = express.Router();

router
  .route("/")
  .post(uploadSliderImages, resizeSliderImage, createSlider)
  .get(getAllSlider);
router.route("/:id").delete(deleteSlider);

module.exports = router;
