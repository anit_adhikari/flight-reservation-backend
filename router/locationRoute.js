const express = require("express");
const {
  createLocation,
  deleteLocation,
  getAllLocation,
  getLocation,
  updateLocation,
} = require("../controller/locationController");

const router = express.Router();

router.route("/").get(getAllLocation).post(createLocation);
router
  .route("/:id")
  .delete(deleteLocation)
  .put(updateLocation)
  .get(getLocation);

module.exports = router;
