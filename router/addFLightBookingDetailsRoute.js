const express = require("express");
const {
  createFlightBooking,
  deleteFlightBooking,
  getAllFlightBooking,
  getFlightBooking,
  updateFlightBooking,
} = require("../controller/addFlightBookingDetails");

const router = express.Router();

router.route("/").get(getAllFlightBooking).post(createFlightBooking);
router
  .route("/:id")
  .delete(deleteFlightBooking)
  .put(updateFlightBooking)
  .get(getFlightBooking);

module.exports = router;
