const express = require("express");
const {
  createProduct,
  getAllProduct,
  uploadProductImages,
  resizeProductImage,
  deleteProduct,
  updateProduct,
  getProduct
} = require("../controller/productController");

const router = express.Router();

router
  .route("/")
  .get(getAllProduct)
  .post(uploadProductImages, resizeProductImage, createProduct);
router.route("/:id").delete(deleteProduct).put(updateProduct).get(getProduct)

module.exports = router;
