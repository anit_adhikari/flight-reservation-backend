const express = require("express");
const {
  createCategory,
  deleteCategory,
  updateCategory,
  getAllCategory,
  resizeCategoryImage,
  uploadCategoryImages
} = require("../controller/categoryController");

const route = express.Router();

route.route("/").get(getAllCategory).post(uploadCategoryImages,resizeCategoryImage, createCategory);
route.route("/:id").put(updateCategory).delete(deleteCategory);

module.exports = route;
