const express = require("express");
const {getAllRole,getRole,deleteRole,updateRole,createRole} = require("../controller/userRoleController")
const router = express.Router();

router.route('/').get(getAllRole).post(createRole);
router.route("/:id").put(updateRole).delete(deleteRole).get(getRole);

module.exports = router;
