const express = require("express");
const {
  createFlight,
  deleteFlight,
  getAllFlight,
  getFlight,
  updateFlight,
} = require("../controller/addFlightDetailsController");

const router = express.Router();

router.route("/").get(getAllFlight).post(createFlight);
router.route("/:id").delete(deleteFlight).put(updateFlight).get(getFlight);

module.exports = router;
