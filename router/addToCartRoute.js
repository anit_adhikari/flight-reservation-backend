const express = require("express");
const {
  createAddToCart,
  getAllAddToCart,
  getByUser,
  resizeCategoryImage,
  uploadCategoryImages,
} = require("../controller/addToCartController");

const route = express.Router();

route
  .route("/")
  .post(uploadCategoryImages, resizeCategoryImage, createAddToCart)
  .get(getAllAddToCart);
route.route("/:userId").get(getByUser);

module.exports = route;
