const crypto = require("crypto");
const { promisify } = require("util");
const jwt = require("jsonwebtoken");
const User = require("../modal/usersModal");
const AppError = require("../utils/appError");
const catchAsync = require("../utils/catchAsync");
const { use } = require("../router/userRouter");

const signToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
};

// for signup users
exports.signup = catchAsync(async (req, res, next) => {
  const newUser = await User.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    passwordConfirm: req.body.passwordConfirm,
    role: req.body.role,
  });
  const token = signToken(User._id);
  res.status(201).json({
    status: "success",
    token,
    data: {
      newUser,
    },
  });
});

// for login user
exports.login = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;
  console.log(req.body);
  // check if email and password exist
  if (!email || !password) {
    return next(new AppError("Please Provide email and password!", 400));
  }
  // check users exist and password is incorrect
  const user = await User.findOne({ email }).select([
    "-password",
    "-passwordConfirm",
    "-__v",
  ]);
  if (!user && !(await user.correctPassword(password, user.password))) {
    return next(new AppError("Incorrect email or password", 401));
  }
  // if everything is ok send token to client
  const token = signToken(user._id);
  res.status(201).json({
    status: "success",
    token,
    data: {
      user,
    },
  });
});

// for protect route
exports.protect = catchAsync(async (req, res, next) => {
  // 1) Getting token and check of it's there
  let token;
  if (req.headers.authorization && req.headers.authorization.split(" ")[1]) {
    token = req.headers.authorization.split(" ")[1];
  }
  if (!token) {
    return next(
      new AppError("You are not login! Please login to get access", 401)
    );
  }

  // 2)verification token
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

  // 3) check if users still exist

  const currentUser = await User.findOne({ _id: decoded.id });

  if (!currentUser) {
    return next(
      new AppError("The user belong to this token no longer exist", 401)
    );
  }

  // 4) check if users chenged password after the token was issued
  if (currentUser.changedPasswordAfter(decoded.iat)) {
    return next(
      new AppError("Users recently changed password! Please login again", 401)
    );
  }

  // grand access to protected route
  req.user = currentUser;

  next();
});

// for restrice users
exports.restrictTo = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(
        new AppError("you do not have permission to perform this action", 403)
      );
    }
    next();
  };
};

// @desc get current login in user
// @route post /api/v1/auth/me
// @access Private

exports.getMe = catchAsync(async (req, res, next) => {
  const user = await User.findById(req.user.id).select([
    "role",
    "email",
    "name",
  ]);
  console.log(user);
  res.status(200).json({
    success: true,
    result: user,
  });
  // next();
});
