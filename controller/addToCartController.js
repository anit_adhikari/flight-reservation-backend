const AddToCart = require("../modal/addToCartModal");
const catchAsync = require("../utils/catchAsync");
const factory = require("./handleFactory");
const Product = require("../modal/productModal");
const AppError = require("../utils/appError");
const { v4: uuidv4 } = require("uuid");
const sharp = require("sharp");
const multer = require("multer");

// multer config
const multerStorage = multer.memoryStorage();

const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb(new AppError("Not an image! Please upload only images.", 400), false);
  }
};

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});

exports.uploadCategoryImages = upload.single("image");

exports.resizeCategoryImage = catchAsync(async (req, res, next) => {
  if (!req.files.image) return next();

  // 1) Cover image
  req.body.imageCover = `category-${uuidv4()}-${Date.now()}-cover.jpeg`;
  await sharp(req.files.imageCover[0].buffer)
    .resize(2000, 1333)
    .toFormat("jpeg")
    .jpeg({ quality: 90 })
    .toFile(`uploads/category/${req.body.imageCover}`);

  next();
});

exports.createAddToCart = catchAsync(async (req, res, next) => {
  const userId = req.body.userId;

  let cart = await AddToCart.findOne({ userId });
  // console.log(cart);
  if (cart) {
    let itemIndex = cart.items.findIndex((p) => {
      return p.item._id == req.body.items.item;
    });
    console.log(itemIndex, "itemIndex");
    if (itemIndex > -1) {
      let productIteam = cart.items[itemIndex];
      productIteam.quantity = cart.items[itemIndex].quantity;
      productIteam.price =
        cart.items[itemIndex].item.price * productIteam.quantity;
      cart.items[itemIndex] = productIteam;
    } else {
      let id = { _id: req.body.items.item };
      let product = await Product.findOne(id);

      req.body.items.price = req.body.items.quantity * product.price;
      cart.items.push(req.body.items);
    }

    cart = await cart.save();
    return res.status(201).json({
      status: "success",
      data: cart,
      message: "cart is successfully updated",
    });
  } else {
    let findProduct = await Product.findOne(req.body.item);
    // console.log(findProduct);
    let bodyData = {
      userId: req.body.userId,
      items: {
        item: req.body.items.item,
        price: req.body.items.quantity * findProduct.price,
        quantity: req.body.items.quantity,
      },
    };

    const cart = await AddToCart.create(bodyData);
    res.status(201).json({
      status: "success",
      data: {
        cart,
      },
      message: "Successfully added to cart!!",
    });
  }
});

exports.getAllAddToCart = catchAsync(async (req, res, next) => {
  const cart = await AddToCart.find();
  console.log(cart);
  let count = 0;
  let totalPrice = 0;
  cart.map((data) => {
    data.items.forEach((item) => {
      count += item.quantity * 1;
      totalPrice += item.price * 1;
    });
  });
  res.status(201).json({
    status: "success",
    data: {
      cart,
      totalPrice: totalPrice,
      count: count,
    },
  });
});

// get by user
exports.getByUser = catchAsync(async (req, res, next) => {
  if (!req.params.userId) return next(new AppError("Please provide user Id!"));
  const cart = await AddToCart.find();

  let findCart = cart.find((cart) => {
    if (cart.userId._id == req.params.userId) {
      return cart;
    }
  });
  let count = 0;
  let totalPrice = 0;
  findCart.items.map((item) => {
    count += item.quantity;
    totalPrice += item.price;
  });

  res.status(201).json({
    status: "success",
    data: {
      count,
      totalPrice,
      cart: findCart,
    },
    message: "cart is successfully fetched",
  });
});
