const UserRole = require("../modal/usersModal");
const factory = require("./handleFactory");

exports.createRole = factory.createOne(UserRole);
exports.getAllRole = factory.getAll(UserRole);
exports.getRole = factory.getOne(UserRole);
exports.deleteRole = factory.deleteOne(UserRole);
exports.updateRole = factory.updateOne(UserRole)
