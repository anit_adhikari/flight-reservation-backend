const catchAsync = require("../utils/catchAsync");

const sendEmail = require("../utils/email");
const FlightDetails = require("../modal/addFlightDetails");
const stripe = require("stripe")(process.env.STRIPE_SECERETE_KEY);

exports.getCheckoutSession = catchAsync(async (req, res, next) => {
  const flight = await FlightDetails.findById(req.params.bookingId);
  sendEmail({
    to: "bichar24@gmail.com",
    subject: "hello there",
    text: "how have you been",
  });
  console.log(flight);
  console.log(req.params);
  const session = await stripe.checkout.sessions.create({
    payment_method_types: ["card"],
    success_url: `http://localhost:3000/`,
    cancel_url: `http://localhost:3000/flightinfo`,
    customer_email: req.user.email,
    client_reference_id: req.params.bookingId,
    line_items: [
      {
        name: `${flight.name} Flight Name`,
        currency: "usd",
        amount: flight.price * 100 * req.params.noOfPassanger,
        quantity: req.params.noOfPassanger,
      },
    ],
  });

  res.status(200).json({ status: "success", session });
});
