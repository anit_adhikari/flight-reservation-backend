const Category = require("../modal/categoryModal");
const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const { v4: uuidv4 } = require("uuid");
const multer = require("multer");
const sharp = require("sharp")
const multerStorage = multer.memoryStorage();

const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb(new AppError("Not an image! Please upload only images.", 400), false);
  }
}

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});

exports.uploadCategoryImages = upload.single('image');

exports.resizeCategoryImage = catchAsync(async (req, res, next) => {
  
  if (!req.file ) return next();
  req.body.imageCover = `category-${uuidv4()}-${Date.now()}-cover.jpeg`;
  await sharp(req.file.buffer)
  .resize(2000, 1333)
  .toFormat("jpeg")
  .jpeg({ quality: 90 })
  .toFile(`uploads/${req.body.imageCover}`);
  
  next();
})


exports.getAllCategory = catchAsync(async (req, res, next) => {
  const category = await Category.find();
  res.status(200).json({
    status: "success",
    result: category.length,
    message: "Category have successfully fetched!",
    data:category
  });
});

exports.createCategory = catchAsync(async (req, res, next) => {
  console.log(req.body)
  const category = await Category.create(req.body);
  res.status(201).json({
    status: "success",
    data: {
      category,
    },
    message: "Category have successfully created!",
  });
});

exports.deleteCategory = catchAsync(async (req, res, next) => {
  const category = await Category.findByIdAndDelete(req.params.id);
  if (!category) {
    return next(new AppError("No category found with that id ", 404));
  }
  res.status(201).json({
    status: "success",
    data: null,
    message: "Product have successfully deleted",
  });
});

exports.updateCategory = catchAsync(async (req, res, next) => {
  const category = await Category.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });
  if (!category) {
    return next(new AppError("No category found with that id", 404));
  }
  res.status(200).json({
    status: "success",
    data: {
      category,
    },
    message: "Product have successfully updated!",
  });
});
