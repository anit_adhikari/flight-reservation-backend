const FlightDetailsBooking = require("../modal/addFlightBookingDetails");
const APIFeatures = require("../utils/apiFeatures");
const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const sharp = require("sharp");
const multer = require("multer");
const factory = require("./handleFactory");
const { v4: uuidv4 } = require("uuid");
const multerStorage = multer.memoryStorage();

exports.getAllFlightBooking = factory.getAll(FlightDetailsBooking);
exports.createFlightBooking = factory.createOne(FlightDetailsBooking);
exports.deleteFlightBooking = factory.deleteOne(FlightDetailsBooking);
exports.updateFlightBooking = factory.updateOne(FlightDetailsBooking);
exports.getFlightBooking = factory.getOne(FlightDetailsBooking);
