const FlightDetails = require("../modal/addFlightDetails");
const APIFeatures = require("../utils/apiFeatures");
const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const sharp = require("sharp");
const multer = require("multer");
const factory = require("./handleFactory");
const { v4: uuidv4 } = require("uuid");
const multerStorage = multer.memoryStorage();

exports.getAllFlight = factory.getAll(FlightDetails);
exports.createFlight = factory.createOne(FlightDetails);
exports.deleteFlight = factory.deleteOne(FlightDetails);
exports.updateFlight = factory.updateOne(FlightDetails);
exports.getFlight = factory.getOne(FlightDetails);
