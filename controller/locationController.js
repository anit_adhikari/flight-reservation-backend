const LocaltionModal = require("../modal/LocaltionModal");
const factory = require("./handleFactory");

exports.getAllLocation = factory.getAll(LocaltionModal);
exports.createLocation = factory.createOne(LocaltionModal);
exports.deleteLocation = factory.deleteOne(LocaltionModal);
exports.updateLocation = factory.updateOne(LocaltionModal);
exports.getLocation = factory.getOne(LocaltionModal);
