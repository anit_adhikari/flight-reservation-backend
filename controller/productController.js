const Product = require("../modal/productModal");
const APIFeatures = require("../utils/apiFeatures");
const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const sharp = require("sharp");
const multer = require("multer");
const factory = require("./handleFactory");
const { v4: uuidv4 } = require("uuid");
const multerStorage = multer.memoryStorage();

const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb(new AppError("Not an image! Please upload only images.", 400), false);
  }
};

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});

exports.uploadProductImages = upload.fields([
  { name: "imageCover", maxCount: 1 },
  { name: "images", maxCount: 12 },
]);

exports.resizeProductImage = catchAsync(async (req, res, next) => {
  if (!req.files.imageCover || !req.files.images) return next();

  // 1) Cover image
  req.body.imageCover = `product-${uuidv4()}-${Date.now()}-cover.jpeg`;
  await sharp(req.files.imageCover[0].buffer)
    .resize(2000, 1333)
    .toFormat("jpeg")
    .jpeg({ quality: 90 })
    .toFile(`uploads/${req.body.imageCover}`);

  // 2)Images
  req.body.images = [];

  await Promise.all(
    req.files.images.map(async (file, i) => {
      //  Name for images
      const filename = `product-${uuidv4()}-${Date.now()}-${i + 1}.jpeg`;
      // resize images
      await sharp(file.buffer)
        .resize(2000, 1333)
        .toFormat("jpeg")
        .jpeg({ quality: 90 })
        .toFile(`uploads/${filename}`);

      req.body.images.push(filename);
    })
  );
  console.log(req.body.images);
  next();
});

// exports.getAllProduct = catchAsync(async (req, res, next) => {
//   const features = new APIFeatures(Product.find(), req.query)
//     .filter()
//     .sort()
//     .limitFields()
//     .paginate();
//   const product = await features.query;
//   res.status(200).json({
//     status: "success",
//     results: product.length,
//     data: {
//       product,
//     },
//     message: "Product have successfully fetched!",
//   });
// });

// exports.createProduct = catchAsync(async (req, res, next) => {
//   const product = await Product.create(req.body);

//   console.log(req.body)
//   res.status(201).json({
//     status: "success",
//     data: {
//       product: product,
//     },
//     message: "Product have successfully fetched!",
//   });
// });

// exports.deleteProduct = catchAsync(async (req, res, next) => {
//   const product = await Product.findByIdAndDelete(req.params.id);
//   if (!product) {
//     return next(new AppError("no product found with that id", 404));
//   }
//   res.status(204).json({
//     status: "success",
//     data: null,
//   });
// });

// exports.updateProduct = catchAsync(async (req, res, next) => {
//   const product = await Product.findByIdAndUpdate(req.params.id, req.body, {
//     new: true,
//     runValidators: true,
//   });
//   if (!product) {
//     return next(new AppError("no product found with that id", 404));
//   }
//   res.status(200).json({
//     status: "success",
//     data: {
//       product,
//     },
//   });
// });

exports.getAllProduct = factory.getAll(Product);
exports.createProduct = factory.createOne(Product);
exports.deleteProduct = factory.deleteOne(Product);
exports.updateProduct = factory.updateOne(Product);
exports.getProduct = factory.getOne(Product);
