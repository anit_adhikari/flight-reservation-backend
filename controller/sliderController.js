const { v4: uuidv4 } = require("uuid");
const sharp = require("sharp");
const multer = require("multer");
const Slider = require("../modal/sliderModal");
const AppError = require("../utils/appError");
const catchAsync = require("../utils/catchAsync");
const factory = require("./handleFactory");
// multer config
const multerStorage = multer.memoryStorage();

const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb(new AppError("Not an image! Please upload only images.", 400), false);
  }
};

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});

exports.uploadSliderImages = upload.single('image')

exports.resizeSliderImage = catchAsync(async (req, res, next) => {
  // console.log(req.body)
  // console.log(req.file)
  if (!req.file) return next();
  console.log(req.file.buffer)
  req.body.images = `slider-${uuidv4()}-${Date.now()}-cover.jpeg`;
  await sharp(req.file.buffer)
  .resize(2000, 1333)
  .toFormat("jpeg")
  .jpeg({ quality: 100 })
  .toFile(`uploads/slider/${req.body.images}`);
  
  console.log(req.body)
  next();
});

exports.createSlider = factory.createOne(Slider);
exports.deleteSlider = factory.deleteOne(Slider);
exports.getAllSlider = factory.getAll(Slider);
