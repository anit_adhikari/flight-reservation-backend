const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const AppError = require("./utils/appError");
const productRoute = require("./router/productRouter");
const userRouter = require("./router/userRouter");
const globalErrorHandler = require("./controller/errorController");
const categoryRoute = require("./router/categoryRoute");
const addToCartRoute = require("./router/addToCartRoute");
const sliderRoute = require("./router/sliderRoute");
const reviewRouter = require("./router/reviewRoute");
const userRoleRouter = require("./router/userRoleRouter");
const flightRoute = require("./router/flightDetailsRoute");
const flightBookingRoute = require("./router/addFLightBookingDetailsRoute");
const locationRoute = require("./router/locationRoute");
const bookingRoute = require("./router/bookingRoute");

const app = express();

// Middleware
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

app.use(express.json());
app.use(express.static(`${__dirname}/uploads`));
app.use(cors());

app.use((req, res, next) => {
  req.requestTime = new Date().toISOString();
  next();
});

// routes middleware
app.use("/api/v1/product", productRoute);
app.use("/api/v1/users", userRouter);
app.use("/api/v1/category", categoryRoute);
app.use("/api/v1/add-to-cart", addToCartRoute);
app.use("/api/v1/slider", sliderRoute);
app.use("/api/v1/review", reviewRouter);
app.use("/api/v1/user-role", userRoleRouter);

// airline reservation route
app.use("/api/v1/flight", flightRoute);
app.use("/api/v1/flight-booking", flightBookingRoute);
app.use("/api/v1/location", locationRoute);
app.use("/api/v1/booking", bookingRoute);

// handle route not founnd
app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server`, 404));
});

app.use(globalErrorHandler);
module.exports = app;
