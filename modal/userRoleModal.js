const moongoose = require("mongoose");

const userRoleModel = new moongoose.Schema({
  name: {
    type: String,
    required: [true, "Name is required field!"],
  },
  status: {
    type: String,
    enum: ["active", "inactive"],
    default: "active",
    required: [true, "Status is required field!"],
  },
  description: {
    type: String,
    required:[true,'Descrption is required field']
  }
});

const UserRole = moongoose.model("UserRole", userRoleModel);
module.exports = UserRole;
