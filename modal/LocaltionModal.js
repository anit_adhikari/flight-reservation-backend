const mongoose = require("mongoose");

const localModalSchema = new mongoose.Schema({
  //   from: {
  //     type: String,
  //     required: [true, "A flight must have from!"],
  //     trim: true,
  //   },
  city: {
    type: String,
    required: [true, "A flight must have to!"],
    trim: true,
  },
  country: {
    type: String,
    required: [true, "A flight must have to!"],
    trim: true,
  },
});

const localModal = mongoose.model("localModal", localModalSchema);
module.exports = localModal;
