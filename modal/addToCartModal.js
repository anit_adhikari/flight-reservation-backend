const mongoose = require("mongoose");

const addToCartSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },

  createdAt: {
    type: Date,
    default: Date.now(),
  },
  items: [
    {
      item: { type: mongoose.Schema.Types.ObjectId, ref: "Product" },
      quantity: { type: Number, default: 0 },
      price: { type: Number, default: 0 },
    },
  ],
});

addToCartSchema.pre(/^find/, function (next) {
  this.populate("userId").populate({
    path: "items",
    populate: {
      path: "item",
    },
  });
  next();
});

const AddToCart = mongoose.model("AddToCart", addToCartSchema);

module.exports = AddToCart;
