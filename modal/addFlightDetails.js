const mongoose = require("mongoose");

const flightDetailsSchema = new mongoose.Schema({
  from: {
    type: String,
    required: [true, "A flight must have from!"],
    trim: true,
  },
  price: {
    type: Number,
    default: 0,
  },
  to: {
    type: String,
    required: [true, "A flight must have to!"],
    trim: true,
  },
  name: {
    type: String,
    required: [true, "A flight must have name!"],
    trim: true,
  },
  weight: {
    type: String,
    required: [true, "A flight must have weight!"],
    trim: true,
  },
  startDate: {
    type: Date,
    required: [true, "A flight must have starting date"],
  },
  endingDate: {
    type: Date,
    required: [true, "A flight must have ending date"],
  },
  time: {
    type: String,
    required: [true, "A flight must have ending date"],
  },
  maxPassanger: {
    type: Number,
    required: [true, "A flight must have ending date"],
    min: 10,
  },
  isInbound: {
    type: Boolean,
    //   required: [true, "A flight must have ending date"],
    min: 10,
  },
  seat: {
    type: Array,
    required: [true, "Seat is required field!"],
  },
});

const FlightDetails = mongoose.model("FlightDetails", flightDetailsSchema);
module.exports = FlightDetails;
