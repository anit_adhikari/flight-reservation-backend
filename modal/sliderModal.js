const mongoose = require("mongoose");

const sliderSchema = new mongoose.Schema({
  title: {
    type: String,
  },
  images: {
    type: String,
    required: [true, "Please upload images"],
  },
});

const Slider = mongoose.model("Slider", sliderSchema);

module.exports = Slider;
