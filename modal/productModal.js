const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "A product must have name!"],
    trim: true,
  },
  price: {
    type: Number,
    default: 0,
  },
  brand: {
    type: String,
  },
  unitSize: {
    type: Number,
  },
  quantity: {
    type: Number,
  },
  discountPercentage: {
    type: Number,
    min: [0, "Discount must be above 0"],
    max: [100, "Discount must be below 100"],
  },
  // discountPrice: {
  //   type: Number,
  // },
  totalPrice: Number,
  status: {
    type: Boolean,
    default: true,
  },
  category: {
    type: Array,
  },
  priceDiscount: {
    type: Number,
    validate: {
      validator: function (val) {
        return val < this.price;
      },
    },
    message: `Discount price ({VALUE}) should be below regular price`,
  },
  description: {
    type: String,
    trim: true,
  },
  imageCover: {
    type: String,
    required: [true, "A product must have image cover"],
  },
  images: [String],
  createdAt: {
    type: Date,
    default: Date.now(),
    select: false,
  },
  ratingQuantity: {
    type: Number,
    default: 0,
  },
  ratingAverage: {
    type: Number,
    default: 4.5,
    min: [1, "Rating must be below 1"],
    max: [5, "Rating must be below 5"],
  },
});

const Product = mongoose.model("Product", productSchema);
module.exports = Product;
