const mongoose = require("mongoose");
const validator = require("validator");

const AddFlightBookingDetailsSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "A flight must have firstName!"],
    trim: true,
  },
  lastName: {
    type: String,
    required: [true, "A flight must have lastName!"],
    trim: true,
  },
  gender: {
    type: String,
    required: [true, "A flight must have gender"],
    trim: true,
    // enum: ["M", "F"],
  },
  country: {
    type: String,
    required: [true, "A flight must have country"],
    trim: true,
  },
  mobileNumber: {
    type: Number,
    required: [true, "A flight must have mobileNumber"],
    trim: true,
  },
  email: {
    type: String,
    required: [true, "Please provide your email"],
    // unique: true,
    lowercase: true,
    validate: [validator.isEmail, "please provide valid email"],
  },
  passfirstName: {
    type: String,
    required: [true, "A flight must have firstName!"],
    trim: true,
  },
  passlastName: {
    type: String,
    required: [true, "A flight must have lastName!"],
    trim: true,
  },
  passgender: {
    type: String,
    required: [true, "A flight must have gender"],
    trim: true,
    // enum: ["M", "F"],
  },
  passcountry: {
    type: String,
    required: [true, "A flight must have country"],
    trim: true,
  },
  passmobileNumber: {
    type: Number,
    required: [true, "A flight must have mobileNumber"],
    trim: true,
  },
  passemail: {
    type: String,
    required: [true, "Please provide your email"],
    // unique: true,
    lowercase: true,
    validate: [validator.isEmail, "please provide valid email"],
  },
  passDocType: {
    type: String,
    required: [true, "Please provide your email"],
  },
  passDocNumber: {
    type: String,
    required: [true, "Please provide your email"],
  },
  noOfPassanger: {
    type: Number,
    required: [true, "A flight must have noOfPassenger"],
  },
});

const AddFlightBookingDetails = mongoose.model(
  "AddFlightBookingDetails",
  AddFlightBookingDetailsSchema
);
module.exports = AddFlightBookingDetails;
